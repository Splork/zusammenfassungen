# -*- coding: utf-8 -*-
"""
___________________________________________________________________
|                                                                 |
|   ALGORITHMEN ZUR VORLESUNG "NUMERISCHE METHODEN FÜR PHYSIKER"  |
|                                                                 |
|   Bei V. GRADINARU an der ETH Zürich - Frühjahrssemester 2013   |
|                                                                 |
|   Zusammengestellt von Jonas Rembser                            |
|_________________________________________________________________|
    Erläuterungen
    - "MDP" steht für "Multidimensional proofed" und signalisiert, 
      dass eine Implementation auch in mehr als einer Dimension funktioniert
"""
import numpy as np
import scipy.linalg as lg
import scipy.optimize as op
from numpy.random import rand
"""
===========================================
|   KAPITEL 0: Konvergenzgeschwindigkeit  |
===========================================

    LINEARE EXPONENTIELLE KONVERGENZ (E(n) = O(p^n)) - KONVERGENZRATE BESTIMMEN
        PARAMETER:
            - err : Abschätzung des Fehlers
        RETURNS:
            Konvergenzrate p bestimmt aus ln(p) = Steigung im Semilogy-Plot
"""
def konvergenzrate(err):
    N = err.size
    x = np.linspace(0, N-1, N)
    return np.exp(np.polyfit(x, np.log(err), 1)[0])
"""
    KONVERGENZORDNUNG SUPERLINEARER EXPONENTIELLER KONVERGENZ:
        RETURNS:
            Die Funktion mittelt die Logarithmusformel zu Anschätzung
            der Konvergenzordnung über alle möglichen Iterationsschritte
"""    
def konvergenzordnung(err):
    N = err.size; p = 0
    for i in xrange(1, N-1):
        tmp = np.log(err[i])
        p += (np.log(err[i+1])-tmp)/(tmp-np.log(err[i-1]))
    return p/(N-2)
"""
    ALGEBRAISCHE KONVERGENZ (E(n) = O(n^(-q))) - EXPONENTEN BESTIMMEN:
"""
def konvergenzexponent(err):
    N = err.size
    x = np.linspace(1, N, N)
    return -np.polyfit(np.log(x), np.log(err), 1)[0]
"""
==================================
|   KAPITEL 1: Nullstellensuche  |
==================================

    FIXPUNKTITERATION:
        PARAMETER (Wiederkehrende Parameter werden nicht nochmal aufgeführt):
            - x0   : Startwert
            - func : Iterationsvorschrift
            - maxit: Maximale Anzahl Iterationsschritte
        RETURNS:
            - Numerischer Fixpunkt x 
            - Anzahl benötigter Schritte k
"""
def fixpoint(x0, func, maxit=50, tol=1e-6):
    for k in xrange(maxit):
        x = func(x0)
        r = abs(x - x0) # mit z.B.absoluter Änderung...
        if(r < tol): return x,k+1# ...as Abbruchbedingung
        x0 = x                   # (Alt. Abbruchkriterien: rel. Änd., Residuum)
    return np.NaN, maxit # Output bei überscheiten der maxit

"""
    NEWTON-VERFAHREN (MDP) / SEKANTEN-VERFAHREN / BISEKTIONSVERFAHREN:
        PARAMETER:
            - x     : Startwert (Bei Sekante zwei Startwerte x0 und x1)
            - F     : Funktion
            - DF    : Ableitung der Funktion
            - a, b  : Intervall, in dem die Nullstelle liegen sollte (a < b)
            - damped: Flag für gedämpftes Newton-Verfahren mit
                      natural monotonicity test
        RETURNS:
            - Numerische Nullstelle x 
            - Anzahl benötigter Schritte k
"""    
def nmtest(F, DF, x): # natural monotonicity test ist ein dependency für newton
    lam = 1.          # nur für 1-d implementiert, für m-d benutze lg.solve()
    for i in xrange(10):
        links = lg.norm(F(x - lam*(F(x)/DF(x)))/DF(x))
        rechts = (1.-lam/2.)*lg.norm(F(x)/DF(x))
        if links <= rechts: return lam
        lam /=2
    return np.NaN

def newton(F, DF, x, maxit=50, tol=1e-12, damped=False):            
    for k in xrange(maxit):
        try: s = lg.solve(DF(x), F(x))    # Bei mehreren Dimensionen...
        except ValueError: s = F(x)/DF(x) # ...oder bei einer
        if damped: lam = nmtest(F, DF, x)
        else: lam = 1
        x -= s * lam
        if(lg.norm(s) < tol *lg.norm(x)): return x,k+1 # Mit rel. Fehler als 
    return np.NaN, maxit                               # Abbruchkriterium
    
def secant(F, x0, x1, maxit=50, tol=1e-12):
    for k in xrange(maxit):
        s = F(x1)*(x1 - x0)/(F(x1) - F(x0))
        x0 = x1; x1 -= s
        if(abs(s) < tol * abs(x1)): return x1,k+1 
    return np.NaN, maxit
    
def bisection(F, a, b, maxit=10**3, tol=1e-12):
    for k in xrange(maxit):    
        x = (a+b)/2.
        if(F(a)*F(x)>0): a = x
        else: b = x
        print x
        if(abs(a-b) < tol): return x, k + 1
    return np.NaN, maxit
"""
==================================
|   KAPITEL 2: Lineare Algebra   |
==================================

    JACOBI-VERFAHREN / GAUSS-SEIDEL-VERFAHREN:
        PARAMETER:
            - A, b : Bilden das zu lösende LGS Ax = b 
            - x    : Startwert
        RETURNS:
            - x    : Lösung des LGS 
"""
def solvejacobi(A,b,x,maxit=10000,tol=1e-2):
    P = np.diag(np.diag(A))
    for k in xrange(maxit):
        t = np.dot(A,x)-b
        x -= np.solve(P,t)
        if np.norm(t) < tol:
            return x, k+1
    return np.NaN,maxit

def solvegaussseidel(A,b,x,maxit=10000,tol=1e-2):
	P = np.tril(A)
	for k in xrange(maxit):
		t = np.dot(A,x)-b
		x -= np.solve(P,t)
		if np.norm(t) < tol:
			return x, k+1
	return np.NaN,maxit 
"""
    QR-SOLVER: Löst das LGS Ax=b mittels QR-Zerlegung
""" 
def solveqr(A,b):
    Q, R = lg.qr(A)
    return lg.solve(R, np.dot(Q.T,b))
"""
    GRAM-SCHMIDT: 
        Orthonormalisirungsverfahren, ist aber numerisch instabil.
        Bessere Alternativen wie die Householder-Transformation mussten seit 
        Beginn der Aufzeichnungen nie in einer Prüfung implementiert werden.
""" 
def qrgramschmidt(A):  # Dies ist eine modifizierte, numerisch günstige Version
    m, n = A.shape
    Q = np.zeros([m,n]); R = np.zeros([n,n])
    V = A.copy()
    for i in xrange(n):
        R[i,i] = lg.norm(V[:,i])
        Q[:,i] = V[:,i]/R[i,i]
        for j in xrange(i+1,n):
            R[i,j] = np.dot(Q[:,i],V[:,j])
            V[:,j] = V[:,j] - R[i,j]*Q[:,i]
    return Q, R
"""
====================================
|   KAPITEL 3: Ausgleichsrechnung  |
====================================
    ALGORITHMEN ZU LÖSUNG DES LINEAREN LEAST-SQUARE-PROBLEMS Ax=b:       
"""
def lsqnormal(A,b): # schlecht konditioniert, da Matrixquadrate vorkommen
    return np.linalg.solve(np.dot(A.T,A),np.dot(A.T,b))

def lsqqr(A,b): # Der Ansatz mit QR-Zerlegung und der mit SVD sind besser 
    Q,R = lg.qr(A)
    x = np.linalg.solve(R, np.dot(Q.T, b))    
    return x
"""
    eps : Präzisionsgrenze, ab der die Werte noch als null anerkannt werden
"""    
def lsqsvd(A,b,eps=1e-6):
    U,s,Vh = lg.svd(A)
    r = 1+np.where(s/s[0]>eps)[0].max() #numerical rank
    x = np.dot(Vh[:,:r].T,np.dot(U[:,:r].T,b)/s[:r])
    return x
"""
    GAUSS-NEWTON-VERFAHREN:
        PARAMETER:
            - F  : Die Funktion unseres nichtlinearen Ausgleichproblems 
            - DF : Der Gradient dieser Funktion
            - z  : Startwert
        RETURNS:
            - z  : Die numerische Lösung des least-squares-Problems (F(z) min.)
"""
def gaussnewton(F,DF,z,maxit=50,tol=1e-7):
	for k in xrange(maxit):
		s = lg.lstsq(DF(z),-F(z))[0]     	# solve linear least squares
		z = z+s
		if lg.norm(s) < tol:
			return z, k + 1
	return np.NaN, maxit

"""
===================================
|   KAPITEL 4: Eigenwertprobleme  |
===================================
    DIE FAST ULTIMATIVE POTENZMETHODE:
        PARAMETER:
            - A       : Quadratische Matrix
            - x       : Startvektor
            - inv     : Setzte 'True' für inverse Potenzmethode
            - shift   : Shiften der inversen Potenzmethode,
                        um einen Eigenwert in der Nähe von 'shift' zu finden
                        macht keinen Sinn bei nicht-inverser Potenzmethode
            - rayleigh: Kann die Konvergenz der inversen geshifteten Methode
                        EXTREM stark beschleunigen oder alles zerstören!
                        YOLO!!!
        RETURNS:
            - ray     : Gefundener Eigenwert (kann so modifiziert werden,
                        dass ray aus jedem Schritt ausgegeben wird)
            - k       : Anzahl benötigter Schritte
        Um die Funktion nicht zu überladen wurde die Möglichkeit einen 
        Vorkonditionierer zu verwenden, vernachlässigt
"""
def eigpow(A,x,inv=False,shift=0,rayleigh=False,maxit=1000,tol=1e-12):    
    B = A - np.eye(x.size)*shift; ray = np.array([0])
    for k in xrange(maxit):
        if inv: w = lg.solve(B,x)
        else: w = np.dot(B,x)
        x = w/lg.norm(w)
        ray = np.append(ray, [np.dot(np.dot(x.T,A),x)])
        if abs(ray[-2]-ray[-1]) < tol: return ray[-1], k + 1 # Entferne [-1]
        if rayleigh: B = A-np.eye(x.size)*ray[-1] # YOLO!!!      # für alle rays
    return np.NaN, maxit
"""
    QR-ALGORITHMUS ZUR BESTIMMUNG ALLER EIGENWERTE
        Gibt zwar alle Eigenwerte aus, ist aber eigentlich 'zu teuer'
"""
def eigqr(A,maxit=1000,tol=1e-12):
    for k in xrange(maxit):    
        Q, R = lg.qr(A)
        A = np.dot(R, Q)
    return np.diag(A)
"""
    ARNOLDI-/LANCZOS-VERFAHREN
        PARAMETER: 
            A   : zu approximierende Matrix
            v0  : Startvektor 
            k   : Anzahl Krylov-Schritte (Dimension des Krylovraumes)
        RETURNS:
            V   : Matrix mit Orthonormalbasis (gross, N*k)
            H   : Hessenbergmatrix (klein, k*k)            
"""
def arnoldi(A, v0, k):
    N = v0.shape[0]
    V = np.zeros((N,k+1)); H = np.zeros((k+1,k))
    V[:,0] = v0/lg.norm(v0)
    for i in xrange(k):
        v0 = np.dot(A,V[:,i])
        for j in xrange(i+1):
            H[j,i] = np.dot(np.conj(V[:,j]),v0)
            v0 -= H[j,i]*V[:,j]
        H[i+1,i] = lg.norm(v0)
        V[:,i+1]=v0/H[i+1,i]
    return V[:,:-1],  H[:-1,:]
"""
    Nimm das Lanczos-Verfahren besser nur für dünnbesetzte oder hermitesche A
"""
def lanczos(A, v0, k):
    N = v0.shape[0]
    V = np.zeros((N,k+1))
    V[:,0] = v0/lg.norm(v0)
    alpha = np.zeros(k); beta = np.zeros(k+1)
    for i in xrange(k):
        v0 = np.dot(A,V[:,i])
        if i>0: v0 -= beta[i]*V[:,i-1]
        alpha[i] = np.dot(np.conj(V[:,i]),v0)
        v0 -= alpha[i]*V[:,i]
        beta[i+1] = lg.norm(v0)
        V[:,i+1] = v0/beta[i+1]
    beta = beta[1:-1]
    H = np.diag(alpha)+np.diag(beta,1)+np.diag(beta,-1)
    return V[:,:-1],  H
"""
===========================================
|   KAPITEL 5: Polynomiale Interpolation  |
===========================================

    Die folgenden Funktionen sind voll kompatibel zu 
    numpy.polyval() und numpy.polyfit()

    INTERPOLATION IN MONOMBASIS:
        Alle Tools die man zu Polynomen in der Monombasis brauchen kann.
        "interpolmonom" interpoliert einfach ein Polynom zu den n x-, y-Werten
        die man ihm gibt und gibt die Koeffizienten des Interpolationspolynoms
        vom Grad n-1 aus, beginnend mit dem führenden Koeffizienten.
        
        "interpolval" wertet einfach ein Polynom mit gegebenen Koeffizienten p
        an den Stellen x aus. Beide Funktionen bedienen sich dazu "monombase".        
"""
def monombase(x, n):
    A = np.ones(n)
    for i in xrange(n-1):
        A = np.vstack((A[0]*x, A))
    return A

def interpolmonom(x, y):
    n = x.size; A = monombase(x, n)
    p = np.linalg.solve(A.T, y)
    return p    

def interpolval(p, x):
    n = x.size; A = monombase(x, n)
    y = np.dot(A.T, np.hstack((np.zeros(n-p.size), p)))
    return y
"""
    "Interpolnewton" berechnet die Gewichte b der Newtonbasis mit den 
    dividierten Differenzen und "interpolhorner" kann das durch diese 
    bestimmte Polynom an den Stellen xx wieder auswerten, muss aber 
    die ursprünglichen Stützstellen x kennen.
"""
def interpolnewton(x,y):
    n = y.shape[0] - 1; b = y.copy()
    if n>0: 
        b[0:n] = interpolnewton(x[0:n], b[0:n])
        for j in xrange(0,n): b[n] = (b[n] - b[j]) / (x[n] - x[j])
    return b
    
def interpolhorner(b,x,xx):
    yy = b[-1]
    for i in xrange(len(b)-2,-1,-1):
        yy = (xx-x[i])*yy + b[i]
    return yy	
"""
    Das folgende ist sowas wie ein numpy.linspace oder np.logspace.
    Er gibt Chebychev-verteile Punkte im Intervall [a, b] aus. Cool, oder?
"""
def chebspace(a, b, n): 
    x = np.linspace(0, n-1, num=n) # Linspace
    x = np.cos((2*(x+1)-1)/(2*n)*np.pi) # Chebychev-Knoten finden
    return (x+1) * (b - a)/2 + a # Skalieren und ausgeben
"""
================================================
|   KAPITEL 6: Trigonometrische Interpolation  |
================================================
    FOURRIER-KOEFFIZIENTEN bekommen 
"""
"""
    AUSWERTEN DER TRIGONOMETRISCHEN INTERPOLATION:
        PARAMETER:
            - y  : Funktionswerte der zu interpolierenden Funktion 
                   an äquidistanten Stuetzstellen im Intervall 
            - N  : Anzahl der equi.-dist. Auswertungspunkte
        RETURNS:
            - v  : Die Werte der trigonometrischen Interpolation 
"""
def evaliptrig(y,N):
    n = len(y)
    if (n%2) == 0:
        c = np.fft.fft(y)*1./n        
        a = np.zeros(N, dtype=complex)
        a[:n/2] = c[:n/2]
        a[N-n/2:] = c[n/2:]
        v = np.fft.ifft(a) * N
        return v
    else: raise TypeError, 'odd_length'
"""
    Diese Methode gibt einfach das n-te Chebychev-Polynom aus an der Stelle x.
"""
def chebpol(n, x): return np.cos(n*np.arccos(x))
"""
    CHEBYCHEV-INETRPOLATION:
        Nutzt 'ifft' um es mit nur O(nlogn) Operationen zu schaffen
        PARAMETER:
            - y  :  Auswertungen der zu Interpolierenden Funktion an chebspace
                    Die Funktion denkt sich das im Interval [0,1] !
        RETURNS:
            - a  : Koeffizienten der Chebychev-Interpolation p(x) = sum(a*T(x))
                   (Auswerten am besten mit Clenshaw-Algorithmus)
"""
def chebexp(y):
    n = y.shape[0] - 1
    t = np.arange(0, 2*n+2)
    z = np.exp(-np.pi*1.0j*n/(n+1.0)*t) * np.hstack([y,y[::-1]])
    c = np.fft.ifft(z)
    t = np.arange(-n,n+2)
    b = np.real(np.exp(0.5j*np.pi/(n+1.0)*t) * c)
    a = np.hstack([ b[n], 2*b[n+1:2*n+1]])
    return a
"""
     Diese Beiden Funktionen werten das durch die Gewichte a gegebene Polynom
     wieder aus, und zwar N äquidistanten Stellen in [-1, 1]
"""    
def chebval(a, N):
    x = np.linspace(-1, 1, N)
    y = np.zeros(x.size)
    for i in xrange(a.size):
        y += a[i] * chebpol(i, x)
    return y   

def clenshaw(a,N):
    x = np.linspace(-1, 1, N)
    n = a.shape[0] - 1
    d = np.tile( np.reshape(a,n+1,1), (x.shape[0], 1) )
    d = d.T
    for j in xrange(n, 1, -1):
        d[j-1,:] = d[j-1,:] + 2.0*x*d[j,:]
        d[j-2,:] = d[j-2,:] - d[j,:]
    y = d[0,:] + x*d[1,:]
    return y
"""
    Weil Gradinaru gerne den Satz von Parseval mag, ist es nicht
    unwahrscheinlich, das man ihn benutzten muss, um die L2([a,b])
    Norm einer Funktion zu berechnen. Das macht folgende Funktion:
        PARAMETER:
            - f   : Funktion
            - a, b: Intervall
            - N   : Anzahl der aequidistanten Stuetzstelle zur
                    Fourrier-Transformation
"""   
def fftL2norm(f, a, b, N=2**20):
    y = f(np.linspace(a, b, N))
    c = np.fft.fftshift(np.fft.fft(y))*1./N
    return sum(abs(c)**2) * (b-a)
"""
========================================
|   KAPITEL 7: Numerische Integration  |
========================================
    N ist ist die Anzahl der Intervalle der Zusammengesetzten Quadratur
    Anzahl der Auswertungen, die die Verfahren brauchen:
        Trapezregel: N + 1
        Mittelpunktsregel: N
        Simpsonregel: 2*N+1
"""
def quadtrapez(f, a, b, N):
    x, h = np.linspace(a, b, N+1, retstep=True)
    return h * (sum(f(x[1:-1])) + (f(x[0])+f(x[-1]))/2)
    
def quadmidpoint(f, a, b, N):
    x, h = np.linspace(a, b, N, endpoint=False, retstep=True)
    return h * sum(f(x + h/2.)) 

def quadsimpson(f, a, b, N):
    x, h = np.linspace(a, b, N, endpoint=False, retstep=True)
    return h/3. * (2*sum(f(x+(h/2.))) + sum(f(x[1:])) + (f(a)+f(b))/2.) 
"""
    Die 2-dimensionale Funktion f2d hat die Form f(x, y) = z
"""
def quadtrapez2d(f2d,a,b,Nx,c,d,Ny):   
    def funcy(y):
        result = np.zeros(y.size)
        for i in xrange(y.size-1):
            funcx = lambda x: f2d(x, y[i])
            result[i] = quadtrapez(funcx, a, b, Nx)
        return result
    return quadtrapez(funcy, c, d, Ny)
    
def quadsimpson2d(f2d,a,b,Nx,c,d,Ny):
    def funcy(y):
        result = np.zeros(y.size)
        for i in xrange(y.size-1):
            funcx = lambda x: f2d(x, y[i])
            result[i] = quadsimpson(funcx, a, b, Nx)
        return result    
    return quadsimpson(funcy, c, d, Ny)
"""
    MONTE-CARLO-QUADRATUR:
        - k   : Anzahl der Zufallszahlen
        - a, b: Arrays aus den Intervallgrenzen
"""
def quadmc(k, f, a, b, d):
    rnd = rand(k, d)
    for i in xrange(d):
        rnd.T[i] = rnd.T[i] * (b[i]-a[i]) + a[i]
    return np.prod(b-a) * 1./k * sum(f(rnd))
"""
=================================================================
|   KAPITEL 8: ODEs: Einschrittverfahren und Erhaltungsgrössen  |
=================================================================
    Zuerst kommen impliziter und expliziter Euler und implizite
    Mittelpunktsregel zum Loesen einer ODE ersten Grades (MDP)
        PARAMETER:
            - f      : Funktion der Differentialgleichung y' = f(t, y)
            - t0, y0 : Startwerte
            - h, N  : Schrittweite und Schritzahl
        RETURNS:
            - t, y  : Die Lösung der Diff.-gleichung y(t) an den Stellen t
"""
def odeEE(f, t0, y0, h, N):
    t = np.linspace(t0, h*N+t0, N+1); y = np.array([y0])
    for i in xrange(N):
        y = np.vstack((y,y[-1]+h*f(t[i], y[-1])))
    return t, y.T[0]
    
def odeIE(f, t0, y0, h, N):
    t = np.linspace(t0, h*N+t0, N+1); y = np.array([y0])
    for i in xrange(N):
        stepfunc = lambda z: z - h*f(t[i+1], z) - y[-1]
        y = np.vstack((y,op.fsolve(stepfunc, y[-1])))
    return t, y.T[0]
        
def odeIM(f, t0, y0, h, N):
    t = np.linspace(t0, h*N+t0, N+1); y = np.array([y0])
    for i in xrange(N):
        stepfunc = lambda z: y[i]-z+h*f(.5*(t[i]+t[i+1]),.5*(z+y[i]))
        y = np.vstack((y,op.fsolve(stepfunc, y[-1])))
    return t, y.T[0]
"""
    STÖRMER-VERLET-VERFAHREN:
        PARAMETER:
            - f      : Funktion der Differentialgleichung y'' = f(t, y)
            - t0, y0 : Startwerte
            - y1     : zweiter Startwert, kann auch ydot0 sein,
                       wenn mann dann die ydot0flag auf True setzt
            - h, N  : Schrittweite und Schritzahl
        RETURNS:
            - t, y  : Die Lösung der Diff.-gleichung y(t) an den Stellen t
"""    
def odeSV(f, t0, y0, y1, h, N, ydot0flag=False):
    if ydot0flag:
        y1 = h*y1 + y0 + h**2 *0.5* f(t0, y0)
    y = np.array([y0,y1]); t = np.linspace(t0, h*N+t0, N+1)
    for i in xrange(N-1):
        y = np.append(y,-y[i]+2*y[i+1] + h**2*f(t[i+1],y[i+1]))
    return t,y 
    
#def odeSplitLT():
    
#def odeSplitS():
"""
    RUNGE-KUTTA-VERFAHREN:
      PARAMETER:
        f   - lambda function mit Argumenten (t,y)
        A   - Matrix Butcher-Tableau (Array 2 Dimensionen)
        b   - Unterer Vektor Butcher-Tableau (Array 1 Dimension)
        y0  - Vektor der Anfangswerte zum Zeitpunkt t0 (Array 1 Dimension)
        T   - T=[t0 tEnd] zu approximierendes Intervall (Tuple)
        N   - Anzahl der Iterationsschritte (Zahl)
   
      RETURNS:
        t   - Diskretisierung des Zeitintervalls
        y   - Matrix der approximierten Funktionswerte
"""
def odeRK(f,A,b,y0,T,N):
    s = b.shape[0]
    c = sum(A,1)
    d = np.size(y0)    # Anzahl der Gleichungen = Dimension der Lösung
    t, h = np.linspace(T[0], T[1], N+1, retstep=True) # h = (T[1]-T[0])/N
    y = np.zeros((np.size(t),d))      # Lösungsmatrix
    y[0,:] = y0                 # Anfangswert
    explizit = all(np.diag(A) == 0)    # Exlizit oder implizit?               
    for n in xrange(int(N)):   # Berechnen
        k = np.zeros((s,d))
        for i in xrange(s):
            S = np.zeros(d)
            for j in xrange(s):
                if (j != i):
                    S += A[i][j]*k[j,:]
            if explizit:
                k[i,:] = b[i]*h*f(t[n]+c[i]*h,y[n,:]+S)
            else:
                g1 = lambda x: h*f(t[n]+c[i]*h,y[n] + S + A[i][i]*x) - x
                k[i,:] = b[i]*np.fsolve(g1,k[i-1,:])
        y[n+1,:] = y[n,:] + sum(k,0)
    return t, y
    
"""
============================================
|   KAPITEL 9: Steife Anfangswertprobleme  |
============================================    
    Dei Stabilitätsfunktion S(lam*h) der DGL y' = lam*y
        PARAMETER:
            - A, b  : aus dem Runge-Kutta Verfahren
            - h, lam: Schrittweite und Eigenschaft des Problems
        RETURNS  :
            - S(lam*h) : Wenn grösser eins => Lösung wird exponentiell wachsen
                         sonst             => Lösung ist beschärnkt            
"""
def stability(A, b, h, lam):
    z = lam * h; einsMat = np.eye(b.size)
    return 1 + z * sum(np.dot(b, lg.inv(einsMat - z * A)))